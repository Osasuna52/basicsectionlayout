/* smooth scroll to another section */
var $root = $('html, body');

$('a[href^="#"]').click(function () {
    $root.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);

    return false;
});

/* checking if section is in viewport */
/**
 * Check if an element is out of the viewport
 * @param  {Node}  elem The element to check
 * @return {Object}     A set of booleans for each side of the element
 */
var isOutOfViewport = function (elem) {

    // Get element's bounding
    var bounding = elem.getBoundingClientRect();

    // Check if it's out of the viewport on each side
    var out = {};
    out.top = bounding.top < 0;
    out.left = bounding.left < 0;
    out.bottom = bounding.bottom > (window.innerHeight || document.documentElement.clientHeight);
    out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
    out.any = out.top || out.left || out.bottom || out.right;

    return out;

};

var logViewport = function () {
    var elem1 = document.querySelector("#section1");
    var isOut1 = isOutOfViewport(elem1);
    if (isOut1.bottom) {
        document.getElementById("sec1").classList.remove("active");

    } else {
        document.getElementById("sec1").classList.add("active");

    }
    for (i = 1; i <= 5; i++) {
        var elem = document.querySelector("#section" + i);
        var isOut = isOutOfViewport(elem);
        if (isOut.bottom) {} else {
            document.getElementById("sec1").classList.remove("active");
            document.getElementById("sec2").classList.remove("active");
            document.getElementById("sec3").classList.remove("active");
            document.getElementById("sec4").classList.remove("active");
            document.getElementById("sec5").classList.remove("active");
            document.getElementById("sec" + i).classList.add("active");

        }

    }


};



logViewport();
window.addEventListener('scroll', logViewport, false);